﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedFolk : MonoBehaviour, IOtherFolk {

	LaunchForceCalculator launchForceCalculator;

	public void SetLaunchForceCalculator(LaunchForceCalculator inLaunchForceCalculator) {
		launchForceCalculator = inLaunchForceCalculator;
	}

	public void BeginCountdown (float timeToGetAway)
	{
		GetComponent<Rigidbody2D> ().AddForce (
			launchForceCalculator.CalculateLaunchForce(transform.position),
			ForceMode2D.Impulse);

		StartCoroutine (CountdownToGetAway (timeToGetAway));
	}

	IEnumerator CountdownToGetAway (float timeToGetAway)
	{

		yield return new WaitForSeconds (timeToGetAway);

		Destroy (gameObject);
	}
}
