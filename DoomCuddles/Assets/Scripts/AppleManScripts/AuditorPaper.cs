﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuditorPaper : MonoBehaviour {

	private float theta;
	private float radius;
	// How fast the paper will rotate in rads/second
	private float angularVelocity;
	// Scale factor for radius.  The larger it is, the quicker the radius will grow
	private float radiusScaleFactor;
	// Additive factor that will increase the radii in the spiral
	private float radiusOffset;
	private float inititalTheta;
	private int rotationDirection;

	private Vector2 nextPosition;

	private float piHalves = Mathf.PI * 0.5f;

	private Rigidbody2D rb2d;

	public void BeginSpiral(int inRotationDirection, float initialTheta, float inAngularVelocity,
		float inRadiusScaleFactor, float inRadiusOffset) {
		rb2d = GetComponent<Rigidbody2D>();

		rotationDirection = inRotationDirection;
		theta = initialTheta;
		angularVelocity = inAngularVelocity;
		radiusScaleFactor = inRadiusScaleFactor;
		radiusOffset = inRadiusOffset;

		StartCoroutine (SmoothSpiral ());
	}
		
	protected IEnumerator SmoothSpiral ()
	{

		// Rotate constantly
		while ( true ) {

			theta += Time.deltaTime * angularVelocity;
			radius = (radiusScaleFactor * theta) + radiusOffset;
			nextPosition = new Vector2 (rotationDirection * radius * Mathf.Cos (theta),
				rotationDirection * radius * Mathf.Sin (theta));
			transform.up = nextPosition - rb2d.position;
			rb2d.MovePosition (nextPosition);

			yield return null;
		}
			
	}
}
