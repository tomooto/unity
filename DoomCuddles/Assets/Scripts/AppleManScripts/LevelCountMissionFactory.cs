﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelCountMissionFactory : MonoBehaviour, IMissionFactory {

	public GameObject levelCountMissionPrefab;

	private static int lowerLevelIndex = 3; // Minimum level for misison objective
	private static int upperLevelIndex = 10; // Maximum level for mission objective

	private GameObject tempMissionObject;

	public GameObject generateMission(int missionIndex) {
		tempMissionObject = Instantiate (levelCountMissionPrefab);
		tempMissionObject.GetComponent<LevelCountMission> ()
			.setOptions (Random.Range(lowerLevelIndex, upperLevelIndex), missionIndex);
		return tempMissionObject;
	}

}
