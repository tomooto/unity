﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoCharactersInARowMission : MonoBehaviour {

	private int charactersAppledSinceTouchCount = 0;
	private int countForSuccess = 2;

	void Awake() {
		EventHandler.ScreenTouchedEvent += resetCharactersAppledCount;
		EventHandler.CharacterAppledEvent += checkTwoInARowObjective;
	}

	public void resetCharactersAppledCount(Vector2 touchPosition) {
		charactersAppledSinceTouchCount = 0;
	}

	public void checkTwoInARowObjective(GameObject characterAppled) {
		charactersAppledSinceTouchCount++;
		if (charactersAppledSinceTouchCount >= countForSuccess) {
			Debug.Log ("Hooray!  Mission complete!");
		}
	}

	void OnDestory() {
		EventHandler.ScreenTouchedEvent -= resetCharactersAppledCount;
		EventHandler.CharacterAppledEvent -= checkTwoInARowObjective;
	}
}
