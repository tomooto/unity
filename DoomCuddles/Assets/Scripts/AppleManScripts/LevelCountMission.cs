﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCountMission: MonoBehaviour {

	private int levelForSuccess;
	private int missionIndex;

	public void setOptions(int inLevelForSuccess, int inMissionIndex) {
		levelForSuccess = inLevelForSuccess;
		missionIndex = inMissionIndex;
		EventHandler.LevelCompletedEvent += checkMissionObjective;
	}

	void checkMissionObjective(int levelCompleted) {
		if (levelCompleted >= levelForSuccess) {
			EventHandler.ReportMissionCompleted (missionIndex);
		}
	}

	void OnDestroy() {
		EventHandler.LevelCompletedEvent -= checkMissionObjective;
	}
}
