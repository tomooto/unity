﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleSwordMan : MonoBehaviour, IOtherCharacter {

	private bool isMoving = false;
	private float rotateScaleFactor = 200;		//The higher this is, the faster CSM rotates

	// Use this for initialization
	void Start () {
		StartCoroutine (SmoothMovement ());
	}

	public string getName() {
		return "BoBerry";
	}

	protected IEnumerator SmoothMovement ()
	{
		isMoving = true;

		// Rotate constantly
		while ( true ) {

			transform.rotation = transform.rotation * Quaternion.Euler (0, 0, rotateScaleFactor * Time.deltaTime);

			yield return null;
		}
			
	}
}
