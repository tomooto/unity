﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelInstantiator {

	public static void InstantiateLevelArrays(GameManagerScript gameManager) {

		List<EnemyInfo> tempEnemyInfo = new List<EnemyInfo> ();

		// Beginning Levels

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1), -160,
			gameManager.seekerMelonPrefab));
		gameManager.beginningLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo>();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1), 0, gameManager.circleSwordManPrefab));
		gameManager.beginningLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2), 0, gameManager.shoutManPrefab));
		gameManager.beginningLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 1), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1,1)));
		gameManager.beginningLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 1), 180, gameManager.peaMediumPrefab));
		gameManager.beginningLevels.Add (new Level (tempEnemyInfo));

		// Medium Levels

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 1), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 2), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 3), 0, gameManager.peaLongPrefab));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 3), 180, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), -10, gameManager.seekerMelonPrefab));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 1.2f), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0.5f, 1.2f), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-0.5f, 1.2f), 0, gameManager.shoutManPrefab));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2), 0, gameManager.shieldManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 1), 0, gameManager.shoutManPrefab));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 1), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2, 1)));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 0, gameManager.shoutManPrefab));
		gameManager.mediumLevels.Add (new Level (tempEnemyInfo));

		// Late Levels

		tempEnemyInfo = new List<EnemyInfo>();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 3), -135, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2), -135, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 3), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2,0)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 1), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.circleSwordManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo>();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -0.5f), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.25f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.75f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 0f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 1f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1.5f), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 0), 0, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 1), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo>();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -0.5f), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.25f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.75f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 0f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 1f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1.5f), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), -90, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 0), 0, gameManager.seekerMelonPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo>();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -0.5f), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.25f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1.5f, -1.75f), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 0f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 1f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1.5f), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2.5f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 1), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -0.25f), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), -10, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 30, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 1), 150, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 1), -165, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 25, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), -35, gameManager.seekerMelonPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 2), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 0), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1.0f, -0.5f), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1.0f, -0.5f), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, -0.5f), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2,2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 0), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2,0)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 1), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2,1)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 3), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -3), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -1), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 1), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -3), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 1), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -1), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2,2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 0), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2,0)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 1), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2,1)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2,3)));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -3), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -1), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 1), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -3), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 1), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -1), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2,2)));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 0), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 1.25f), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -1.25f), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 0), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 1.25f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -1.25f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0.5f, -2), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-0.5f, -2), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0.5f, 2), 0, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-0.5f, 2), 0, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 3), 0, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 3), 90, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -3), -90, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -3), 90, gameManager.seekerMelonPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 0), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 1.25f), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -1.25f), -90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 0), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 1.25f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -1.25f), 90, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0.5f, -2), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-0.5f, -2), 180, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0.5f, 2), 0, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-0.5f, 2), 0, gameManager.peaMediumPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 3), 0, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 3), 90, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -3), -90, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -3), 90, gameManager.seekerMelonPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 2.25f), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 2.25f), 180, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1.75f, 2.25f), 0, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -2.25f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, -2.25f), 0, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1.75f, -2.25f), 180, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -1), 0, gameManager.circleSwordManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 2), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2, -2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 3), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 2), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2, -2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, -3), 0, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1, -3)));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -3), 20, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 3), 160, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -2), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -3), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 2), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 3), 0, gameManager.circleSwordManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (0, 2), 180, gameManager.seekerMelonPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -1.75f), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -1.75f), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 0), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 0), -90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 1.75f), 90, gameManager.peaLongPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 1.75f), -90, gameManager.peaLongPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 0), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(0, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -1), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 1), 0, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 0, gameManager.circleSwordManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 3), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-1, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, -3), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-1, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 3), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, -3), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(1, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 0), 180, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 0), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 180, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2, 3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 2), 270, gameManager.shieldManPrefab, gameManager.getGridCoordinates(2, -2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -2), 90, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2, 2)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 90, gameManager.shieldManPrefab, gameManager.getGridCoordinates(-2, -3)));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 2), 180, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2), 180, gameManager.circleSwordManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, 3), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, 3), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, 2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-2, -3), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(-1, -2), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(2, -3), 90, gameManager.circleSwordManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(1, -2), 0, gameManager.shoutManPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -3), 0, gameManager.shoutManPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, -2), 0, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, -3), -90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-2, 2), 180, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (-1, 3), -90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, -2), 0, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, -3), 90, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (2, 2), 180, gameManager.peaShortPrefab));
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates (1, 3), 90, gameManager.peaShortPrefab));
		gameManager.lateLevels.Add (new Level (tempEnemyInfo));

		// Boss Levels

		tempEnemyInfo = new List<EnemyInfo> ();
		tempEnemyInfo.Add (new EnemyInfo (gameManager.getGridCoordinates(0, 0), 180, gameManager.auditorPrefab));
		gameManager.bossLevels.Add (new Level (tempEnemyInfo));

	}
}
