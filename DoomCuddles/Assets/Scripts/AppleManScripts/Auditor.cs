﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Auditor : MonoBehaviour, IHasOptions, IOtherCharacter {

	public GameObject auditorPaperPrefab;
	public GameObject guardianPostPrefab;
	public GameObject barrierPrefab;

	private float radiusOffset = 1f; // Additive factor that will increase the radii in the spiral
	private float angularVelocity = Mathf.PI / 2; // How fast the papers will rotate around the spiral in rads/second
	private float radiusScaleFactor = 1 / Mathf.PI; // Scale factor for radius.  The larger it is, the quicker the radius will grow
	private float instantiationPeriod; // Amount of time between instantiations
	private int paperIndex = 0; // Index for accessing paper reference array
	private float destroyPeriod; // Amount of time for paper to be destroyed

	public int currNumPosts;

	private GameManagerScript gameManager;
	private EventHandler eventHandler;
	private GameObject[] paper = new GameObject[25];
	private GameObject[] guardianPosts = new GameObject[4];
	private GameObject[] barriers = new GameObject[5];

	private bool isSecondRound = false;

	// Use this for initialization
	void Start () {
		gameManager = transform.parent.gameObject.GetComponent<GameManagerScript>();
		currNumPosts = 4;

		instantiationPeriod = Mathf.PI / (2 * angularVelocity);
		destroyPeriod = instantiationPeriod * paper.Length;

		DestroyAllAuditorObjects ();

		StartCoroutine (ThrowPaper());

		SpawnGuardians ();

		barriers [0] = Instantiate (barrierPrefab, transform.position + new Vector3 (0f, 0.6f, 0f),
			Quaternion.AngleAxis (0, Vector3.forward), transform.parent);
		barriers [1] = Instantiate (barrierPrefab, transform.position + new Vector3 (0.5f, 0.45f, 0f),
			Quaternion.AngleAxis (-45, Vector3.forward), transform.parent);
		barriers [2] = Instantiate (barrierPrefab, transform.position + new Vector3 (0.7f, 0.1f, 0f),
			Quaternion.AngleAxis (270, Vector3.forward), transform.parent);
		barriers [3] = Instantiate (barrierPrefab, transform.position + new Vector3 (-0.7f, 0.1f, 0f),
			Quaternion.AngleAxis (90, Vector3.forward), transform.parent);
		barriers [4] = Instantiate (barrierPrefab, transform.position + new Vector3 (-0.55f, 0.45f, 0f),
			Quaternion.AngleAxis (-315, Vector3.forward), transform.parent);
	}

	public void SetOptions(CharacterOptions characterOptions) {
		eventHandler = characterOptions.eventHandler;
		EventHandler.CharacterAppledEvent += UpdateEnemyGotAppled;
	}

	public string getName() {
		return "Ottertor";
	}

	public void UpdateEnemyGotAppled (GameObject enemyThatGotAppled) {
		
		if (enemyThatGotAppled.tag == "AuditorGuardianPost") {
			currNumPosts--;
			Destroy (enemyThatGotAppled);
			if (currNumPosts <= 0) {
				InitiateNextPhase ();
			}
		}

	}

	void InitiateNextPhase() {
		
		if (!isSecondRound) {
			isSecondRound = true;
			SpawnGuardians ();
			currNumPosts = 4;
		} else {
			DestroyBarriers ();
		}

	}

	// Routine that handles shouts, including wait time
	protected IEnumerator ThrowPaper () {

		// Initial wait time to stop paper spawn during countdown
		yield return new WaitForSeconds(0.2f);

		while (true) {

			paper[paperIndex] = Instantiate (auditorPaperPrefab, transform.position + new Vector3(radiusOffset, 0, 0),
				transform.rotation, transform.parent);

			paper [paperIndex].GetComponent<AuditorPaper> ().BeginSpiral (1, 0, angularVelocity, radiusScaleFactor,
				radiusOffset);

			Destroy (paper[paperIndex], destroyPeriod);

			IncrementPaperIndex ();

			yield return new WaitForSeconds (instantiationPeriod);

			paper[paperIndex] = Instantiate (auditorPaperPrefab, transform.position + new Vector3(-radiusOffset, 0, 0),
				transform.rotation, transform.parent);

			paper [paperIndex].GetComponent<AuditorPaper> ().BeginSpiral (-1, 0, angularVelocity, radiusScaleFactor,
				radiusOffset);

			Destroy (paper[paperIndex], destroyPeriod);

			IncrementPaperIndex ();

			yield return new WaitForSeconds (instantiationPeriod);

		}
	}
		
	void OnDestroy() {
//		eventHandler.UnregisterEnemyAppledObserver (this);
		EventHandler.CharacterAppledEvent -= UpdateEnemyGotAppled;
		DestroyAllAuditorObjects ();
	}

	void DestroyAllAuditorObjects() {
		for (int i = 0; i < paper.Length; i++) {
			Destroy (paper [i]);
		}
		for (int i = 0; i < barriers.Length; i++) {
			Destroy (barriers [i]);
		}
		for (int i = 0; i < guardianPosts.Length; i++) {
			Destroy (guardianPosts [i]);
		}
	}

	void IncrementPaperIndex() {
		paperIndex++;
		if (paperIndex >= paper.Length) {
			paperIndex = 0;
		} 
	}

	void SpawnGuardians() {
		for (int i = 0; i < guardianPosts.Length; i++) {
			Destroy (guardianPosts [i]);
		}

		if (isSecondRound) {
			guardianPosts [0] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (0, 3),
				Quaternion.AngleAxis (-15, Vector3.forward), transform.parent);
			guardianPosts [1] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (-2, 0),
				Quaternion.AngleAxis (35, Vector3.forward), transform.parent);
			guardianPosts [2] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (2, 0),
				Quaternion.AngleAxis (-35, Vector3.forward), transform.parent);
			guardianPosts [3] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (0, -3),
				Quaternion.AngleAxis (15, Vector3.forward), transform.parent);
		} else {
			guardianPosts [0] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (2, 3),
				Quaternion.AngleAxis (-25, Vector3.forward), transform.parent);
			guardianPosts [1] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (-2, 3),
				Quaternion.AngleAxis (25, Vector3.forward), transform.parent);
			guardianPosts [2] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (2, -3),
				Quaternion.AngleAxis (-15, Vector3.forward), transform.parent);
			guardianPosts [3] = Instantiate (guardianPostPrefab, gameManager.getGridCoordinates (-2, -3),
				Quaternion.AngleAxis (15, Vector3.forward), transform.parent);
		}
	}

	void DestroyBarriers() {
		for (int i = 0; i < barriers.Length; i++) {
			Destroy (barriers [i]);
		}
	}

}
