﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeekerMelon : MonoBehaviour, IApplerMovementObserver, IHasOptions, IOtherCharacter {

	private float velocity = 1.5f; // This is the actual velocity.
	private float curveRadius = 1.0f; // radius of circle when melon has to turn
	private float deltaDotRight; // Dot product that determines whether appler is to the left or right
	private float deltaDotUp; // Dot product that says whether appler is in front or behind
	private float angularVelocity; // How fast melon will rotate when turning
	private int directionInt; // int that determines which direction melon rotates

	private Vector3 positionsDelta;
	private Vector3 newPosition;
	//
	// Seriously, email unity about this one.  Because it's ridiculous that Lookat doesn't work
	//
	private Quaternion remainingRotationToAppler = new Quaternion(); // Used to perfectly align melon
	private IEnumerator followApplerCoroutine;

	private GameObject applerRef;
	private EventHandler eventHandler;
	private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent <Rigidbody2D> ();
		angularVelocity = Mathf.Rad2Deg * velocity / curveRadius;

		followApplerCoroutine = FollowAppler (applerRef.transform.position);
		StartCoroutine (followApplerCoroutine);
	}
		
	public void SetOptions (CharacterOptions characterOptions) {
		applerRef = characterOptions.applerRef;
		eventHandler = characterOptions.eventHandler;
		eventHandler.RegisterApplerMovementObserver (this);
	}

	public string getName() {
		return "SeekerMelon";
	}
		
	public void updateFromApplerMovement(Vector3 applerNewPosition) {
		StopCoroutine (followApplerCoroutine);
		followApplerCoroutine = FollowAppler (applerNewPosition);
		StartCoroutine (followApplerCoroutine);
	}

	protected IEnumerator FollowAppler (Vector3 applerPosition)
	{

		while (true) {

			positionsDelta = applerPosition - transform.position;
			deltaDotRight = Vector3.Dot (positionsDelta, transform.right);
			deltaDotUp = Vector3.Dot (positionsDelta, transform.up);

			// Use projection onto right axis to determine direction of rotation
			if (deltaDotRight > 0) {
				directionInt = 1;
			} else {
				directionInt = -1;
			}

			// Rotate until aligned with appler (First determines alignment, second check sees if appler
			// "is behind" melon).
			while (deltaDotRight * directionInt > 0 || deltaDotUp < 0) {

				transform.RotateAround (transform.position + directionInt * transform.right * curveRadius,
					Vector3.forward, -directionInt * Time.deltaTime * angularVelocity);

				positionsDelta = applerPosition - transform.position;
				deltaDotRight = Vector3.Dot (positionsDelta, transform.right);
				deltaDotUp = Vector3.Dot (positionsDelta, transform.up);

				yield return null;
			}

			// Perfectly line it up after smooth rotation
			remainingRotationToAppler.SetFromToRotation (transform.up, positionsDelta);
			transform.rotation = transform.rotation * remainingRotationToAppler;

			// Keep moving forward.  This will get interrupted by event emitter if appler moves
			while (true) {
				// Find a new position proportionally closer to the appler, based on the velocity
				newPosition = Vector3.MoveTowards (transform.position, applerPosition,
					velocity * Time.deltaTime);

				// Call MovePosition on attached Rigidbody2D and move it to the calculated position.
				rb2d.MovePosition (newPosition);

				yield return null;
			}

		}
	}

	void OnDestroy() {
		eventHandler.UnregisterApplerMovementObserver (this);
	}
		
}
