﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour, IScreenTouchedObserver {

//	public bool isGamePaused = false;

//	private Vector2 touchPosition;
	private float distanceToTouch;
	private System.Type collisionType;
	private float colliderRadius;
//	private Touch touch;
	private Vector2 tempCurrentPosition;
	private RaycastHit2D[] hitResults = new RaycastHit2D[16]; // raw returned results from circle cast
	private RaycastHit2D[] augHitResults = new RaycastHit2D[16]; // List of hit results that filtered out colliders belonging to the same game object
	private int numAugHitResults = 0; // Index for keeping track of how many results are in augHitResults for this iteration
	private bool isAlreadyInAugHitResults; // Boolean for if a collider in hitResults belongs to another game object in hitResults
	private int numHits;
	private int i;
	private int j;

	private GameManagerScript gameManager;
	private EventHandler eventHandler;
	private LineRenderer lineRenderer;
	private ParticleSystem dustClouds;

	// Use this for initialization
	void Start () {
		colliderRadius = GetComponent<CircleCollider2D> ().radius;
		gameManager = transform.parent.gameObject.GetComponent<GameManagerScript>();
		lineRenderer = transform.GetChild (0).gameObject.GetComponent<LineRenderer> ();
	}

	public void SetOptions(EventHandler inEventHandler) {
		eventHandler = inEventHandler;
		EventHandler.ScreenTouchedEvent += UpdateScreenTouched;
	}

//	void movePlayer() {
	public void UpdateScreenTouched(Vector2 touchPosition) {
		
		tempCurrentPosition = new Vector2 (transform.position.x, transform.position.y);
		distanceToTouch = (touchPosition - tempCurrentPosition).magnitude;
		numHits = Physics2D.CircleCastNonAlloc (tempCurrentPosition, colliderRadius,
			touchPosition - tempCurrentPosition, hitResults, distanceToTouch);
		numAugHitResults = 0;
		// Sort hit results in augHitResults in order to determine which colliders belong to the same game object
		for (i = 0; i < numHits; i++) {
			isAlreadyInAugHitResults = false;
			for (j = 0; j < numAugHitResults; j++) {
				if (hitResults [i].rigidbody.gameObject == augHitResults [j].rigidbody.gameObject) {
					isAlreadyInAugHitResults = true;
					break;
				}
			}
			if (!isAlreadyInAugHitResults) {
				augHitResults [numAugHitResults] = hitResults [i];
				numAugHitResults++;
			}
		}
		// Destory the owning game object if Appleman hit the circle collider first.  Otherwise, destroy Appleman.
		// Unless it's a character leader, then automatically move on.
		for (i = 0; i < numAugHitResults; i++) {
			if (augHitResults [i].collider.GetType () == typeof(CircleCollider2D)) {
				eventHandler.ReportCharacterAppled(augHitResults [i].rigidbody.gameObject);
				if (augHitResults [i].collider.gameObject.tag == "CharacterLeader") {
					break;
				}
			} else if (augHitResults [i].collider.GetType () == typeof(BoxCollider2D)) {
				touchPosition = augHitResults[i].point;
				eventHandler.NotifyApplerRejectedObservers ();
				break;
			}
		}

		transform.position = touchPosition;
		transform.up = touchPosition - tempCurrentPosition;
		lineRenderer.positionCount = 2;
		lineRenderer.SetPositions(new Vector3[] {new Vector3(touchPosition.x, touchPosition.y, 0),
			new Vector3(tempCurrentPosition.x, tempCurrentPosition.y, 0)});

		eventHandler.NotifyApplerMovementObservers (transform.position);

		return;
	}

	void OnTriggerEnter2D(Collider2D other) {
		collisionType = other.GetType();
		if (collisionType == typeof(BoxCollider2D)) {
			eventHandler.NotifyApplerRejectedObservers ();
		}
		if (collisionType == typeof(CircleCollider2D)) {
			eventHandler.ReportCharacterAppled (other.gameObject);
		}
	}

	void OnDestroy() {
		EventHandler.ScreenTouchedEvent -= UpdateScreenTouched;
	}
		
}
