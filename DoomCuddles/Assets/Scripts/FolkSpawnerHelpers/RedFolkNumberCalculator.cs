﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RedFolkNumberCalculator : MonoBehaviour {

	private int lastCalculationIndex = 0; // index last used to calculate the number of folk
	private int[] scoreToNumberBoundaries = new int[] { 5, 10, 25, 40, 50, 75, 100}; // The score that activates the next index
	private int[] maxNumberOfRedFolkValues = new int[] { 1, 1, 2, 3, 4, 5, 6}; // Time for score above.  Arrays must be same length
	private float[] probabilityOfSpawnValues = new float[] {1.0f, 0.25f, 0.5f, 0.5f, 0.5f, 0.4f, 0.4f}; // probability that red folk will spawn

	private int numberToReturn;

	// This gets the next interval of numberOfFolkValues if the new score is greater than the last used boundary
	// and not if the index is about to go out of bounds from the numberOfFolkValues
	public int CalculateNumberOfRedFolk(int currentScore) {
		if (currentScore > scoreToNumberBoundaries[lastCalculationIndex]
			&& lastCalculationIndex < (maxNumberOfRedFolkValues.Length - 1)) {
			lastCalculationIndex++;
		}
		// Use the probabilities to see if red folk actually spawn or not
		numberToReturn = 0;
		for (int i = 0; i < maxNumberOfRedFolkValues [lastCalculationIndex]; i++) {
			if (Random.value <= probabilityOfSpawnValues [lastCalculationIndex]) {
				numberToReturn++;
			}
		}
		return numberToReturn;
	}

	public void ResetCalculator() {
		lastCalculationIndex = 0;
	}
		
}
