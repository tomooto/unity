﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LaunchForceCalculator : MonoBehaviour {

	public GameObject otherFolkPrefab;

	private CoordinateGenerator coordinateGenerator;
	private float initialForceY;
	private float initialForceX;
	private float acceleration;
	private float absAcceleration; // Absolute value of acceleration (since it should always be negative)
	private float maxTime; // Maximum amount of time a folk will be in the air
	private float v0ymax; // Convenience variable for calculating max time
	private float deltaY; // Convenience variable for calculating max time

	// Boundary coordinates are two sets of coordinates ([lower, upper][x,y][1,2])
	private float[,,] yLaunchForceBoundCoordinates = new float[2,2,2];
	private float[,,] xLaunchForceBoundCoordinates = new float[2, 2, 2];
	// Slopes for calculating the launch force based on world position ([lower, upper])
	private float[] yLaunchForceSlopes = new float[2];
	private float[] xLaunchForceSlopes = new float[2];

	public void setCoordinateGenerator(CoordinateGenerator inCoordinateGenerator) {
		Debug.Log ("Setting up generator!");
		coordinateGenerator = inCoordinateGenerator;

		acceleration = otherFolkPrefab.GetComponent<ConstantForce2D> ().force.y;
		absAcceleration = Mathf.Abs (acceleration);

		// This is based off the fact that "3" is the maximum grid coordinate
		yLaunchForceBoundCoordinates[1,1,1] = Mathf.Sqrt (2 * absAcceleration *
			(coordinateGenerator.getMaxCoordinates ().y - coordinateGenerator.getGridCoordinates(new Vector2 (0, 3)).y));
		yLaunchForceBoundCoordinates[0,1,1] = absAcceleration / 3.0f;
		yLaunchForceBoundCoordinates[1,1,0] = 3.0f * absAcceleration / 4.0f;
		yLaunchForceBoundCoordinates[0,1,0] = absAcceleration / 2.0f;

		// Again, based off the fact that "3" is the max grid, and "-1" is the minimum grid spawn point
		yLaunchForceBoundCoordinates[0,0,1] = coordinateGenerator.getGridCoordinates (new Vector2 (0, 3)).y;
		yLaunchForceBoundCoordinates[1,0,1] = coordinateGenerator.getGridCoordinates (new Vector2 (0, 3)).y;
		yLaunchForceBoundCoordinates[0,0,0] = coordinateGenerator.getGridCoordinates (new Vector2 (0, -1)).y;
		yLaunchForceBoundCoordinates[1,0,0] = coordinateGenerator.getGridCoordinates (new Vector2 (0, -1)).y;

		// (y2 - y1) / (x2 - x1)
		for (int i = 0; i < 2; i++) {
			yLaunchForceSlopes [i] = (yLaunchForceBoundCoordinates [i,1,1] - yLaunchForceBoundCoordinates [i,1,0]) /
			(yLaunchForceBoundCoordinates [i,0,1] - yLaunchForceBoundCoordinates [i,0,0]);
		}

		v0ymax = yLaunchForceBoundCoordinates [1, 1, 0];
		deltaY = yLaunchForceBoundCoordinates [1, 0, 1] + coordinateGenerator.getMaxCoordinates ().y;
		maxTime = (-v0ymax - Mathf.Sqrt (v0ymax * v0ymax - 2 * acceleration * deltaY)) / acceleration;

		// Based off of knowing that "-2" is the furthermost x grid coordinate
		// maxDeltaX / t
		xLaunchForceBoundCoordinates [1, 1, 0] = (coordinateGenerator.getMaxCoordinates ().x -
			coordinateGenerator.getGridCoordinates (new Vector2 (-2, 0)).x) / maxTime;
		xLaunchForceBoundCoordinates[0,1,0] = 	(-coordinateGenerator.getMaxCoordinates ().x +
			coordinateGenerator.getGridCoordinates (new Vector2 (2, 0)).x) / maxTime;
		xLaunchForceBoundCoordinates [1, 1, 1] = -xLaunchForceBoundCoordinates [0, 1, 0];
		xLaunchForceBoundCoordinates [0, 1, 1] = -xLaunchForceBoundCoordinates [1, 1, 0];

		xLaunchForceBoundCoordinates [1, 0, 0] = coordinateGenerator.getGridCoordinates(new Vector2 (-2, 0)).x;
		xLaunchForceBoundCoordinates [0, 0, 0] = coordinateGenerator.getGridCoordinates(new Vector2 (-2, 0)).x;
		xLaunchForceBoundCoordinates [1, 0, 1] = coordinateGenerator.getGridCoordinates(new Vector2 (2, 0)).x;
		xLaunchForceBoundCoordinates [0, 0, 1] = coordinateGenerator.getGridCoordinates (new Vector2 (2, 0)).x;

		// (y2 - y1) / (x2 - x1)
		for (int i = 0; i < 2; i++) {
			xLaunchForceSlopes [i] = (xLaunchForceBoundCoordinates [i,1,1] - xLaunchForceBoundCoordinates [i,1,0]) /
				(xLaunchForceBoundCoordinates [i,0,1] - xLaunchForceBoundCoordinates [i,0,0]);
		}
			

//		Debug.Log ("-----------coordinates are----------------");
//		for (int i = 0; i < 2; i++) {
//			for (int j = 0; j < 2; j++) {
//				for (int k = 0; k < 2; k++) {
//					Debug.Log (i.ToString () + j.ToString () + k.ToString ());
//					Debug.Log (xLaunchForceBoundCoordinates[i,j,k]);
//				}
//			}
//		}
//		Debug.Log (xLaunchForceBoundCoordinates);
//		Debug.Log ("--------------------slopes are-----------------");
//		for (int i = 0; i < 2; i++) {
//			Debug.Log (xLaunchForceSlopes[i]);
//		}
	}

	public Vector2 CalculateLaunchForce(Vector2 initialWorldCoordinates) {

		float[] xMaxBounds = {
			xLaunchForceSlopes[0] * (initialWorldCoordinates.x - xLaunchForceBoundCoordinates[0,0,0]) +
				xLaunchForceBoundCoordinates[0,1,0],
			xLaunchForceSlopes[1] * (initialWorldCoordinates.x - xLaunchForceBoundCoordinates[1,0,0]) +
				xLaunchForceBoundCoordinates[1,1,0],
		};

		initialForceY = yLaunchForceSlopes[1] * (initialWorldCoordinates.y - yLaunchForceBoundCoordinates[1,0,0]) +
			yLaunchForceBoundCoordinates[1,1,0];
//		initialForceX = xLaunchForceSlopes[0] * (initialWorldCoordinates.x - xLaunchForceBoundCoordinates[0,0,0]) +
//			xLaunchForceBoundCoordinates[0,1,0];
		initialForceX = xMaxBounds[Random.Range(0,2)];

		initialForceX = Random.Range (
			xLaunchForceSlopes[0] * (initialWorldCoordinates.x - xLaunchForceBoundCoordinates[0,0,0]) +
				xLaunchForceBoundCoordinates[0,1,0],
			xLaunchForceSlopes[1] * (initialWorldCoordinates.x - xLaunchForceBoundCoordinates[1,0,0]) +
				xLaunchForceBoundCoordinates[1,1,0]
		);

		initialForceY = Random.Range (
			yLaunchForceSlopes[0] * (initialWorldCoordinates.y - yLaunchForceBoundCoordinates[0,0,0]) +
				yLaunchForceBoundCoordinates[0,1,0],
			yLaunchForceSlopes[1] * (initialWorldCoordinates.y - yLaunchForceBoundCoordinates[1,0,0]) +
				yLaunchForceBoundCoordinates[1,1,0]
		);
		
		return new Vector2 (initialForceX, initialForceY);
	}
		
}
