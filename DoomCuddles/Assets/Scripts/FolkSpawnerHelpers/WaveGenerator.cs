﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveGenerator : MonoBehaviour {

	public GameObject blueFolkPreviewPrefab;
	public GameObject blueFolkPrefab;
	public GameObject redFolkPreviewPrefab;
	public GameObject redFolkPrefab;

	private float[] gridXs = {-2, -1, 0 , 1, 2};
	private float[] gridYs = {-1, 0, 1, 2, 3};

	private float tempX;
	private float tempY;
	private GameObject previewToReturn;
	private GameObject gameObjectToReturn;

	public Wave generateWave(int numOtherFolk, int numRedFolk, float timeToNextSpawn, float timeToGetAway) {
		
		Wave waveToReturn = new Wave ();
		int[] redFolkIndeces = CreateRedFolkIndeces (numOtherFolk, numRedFolk);

		for (int i = 0; i < numOtherFolk; i++) {
			waveToReturn.wavePackets.Add (makeWavePacket (timeToNextSpawn, timeToGetAway,
				IsRedFolkIndex(i, redFolkIndeces)));
		}

		return waveToReturn;
	}

	private WavePacket makeWavePacket(float timeToNextSpawn, float timeToGetAway, bool isRedFolk) {
		tempX = gridXs [Random.Range (0, gridXs.Length)];
		tempY = gridYs [Random.Range (0, gridYs.Length)];

		if (isRedFolk) {
			previewToReturn = redFolkPreviewPrefab;
			gameObjectToReturn = redFolkPrefab;
		} else {
			previewToReturn = blueFolkPreviewPrefab;
			gameObjectToReturn = blueFolkPrefab;
		}

		return new WavePacket (new Vector3 (tempX, tempY), gameObjectToReturn, previewToReturn,
			timeToNextSpawn, timeToGetAway);
	}

	private int[] CreateRedFolkIndeces(int numOtherFolk, int numRedFolk) {
		int[] redFolkIndecesToReturn = new int[numRedFolk];

		for (int i = 0; i < numRedFolk; i++) {
			redFolkIndecesToReturn [i] = Random.Range (0, numOtherFolk);
		}

		return redFolkIndecesToReturn;
	}

	private bool IsRedFolkIndex(int indexToCheck, int[] redFolkIndeces) {
		for (int i = 0; i < redFolkIndeces.Length; i++) {
			if (indexToCheck == redFolkIndeces [i]) {
				return true;
			}
		}

		return false;
	}
}
