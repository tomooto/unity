﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberOfFolkCalculator : MonoBehaviour {

	private int lastCalculationIndex = 0; // index last used to calculate the number of folk
	private int[] scoreToNumberBoundaries = new int[] { 5, 10, 25, 25}; // The score that activates the next index
	private int[] numberOfFolkValues = new int[] { 4, 6, 8, 10}; // Number of folk for score above.  Arrays must be same length

	// This gets the next interval of numberOfFolkValues if the new score is greater than the last used boundary
	// and not if the index is about to go out of bounds from the numberOfFolkValues
	public int CalculateNumberOfFolk(int currentScore) {
		if (currentScore > scoreToNumberBoundaries[lastCalculationIndex]
			&& lastCalculationIndex < (numberOfFolkValues.Length - 1)) {
			lastCalculationIndex++;
		}
		return numberOfFolkValues [lastCalculationIndex];
	}

	public void ResetCalculator() {
		lastCalculationIndex = 0;
	}
}
