﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToNextSpawnCalculator : MonoBehaviour {

	private int lastCalculationIndex = 0; // index last used to calculate the number of folk
	private int[] scoreToTimeBoundaries = new int[] { 1, 4, 10, 25, 40, 55, 70, 100}; // The score that activates the next index
	private float[] timeToNextSpawnValues = new float[] { 1.0f, 0.75f, 0.5f, 0.4f, 0.3f, 0.25f, 0.15f, 0.1f}; // Time for score above.  Arrays must be same length

	// This gets the next interval of numberOfFolkValues if the new score is greater than the last used boundary
	// and not if the index is about to go out of bounds from the numberOfFolkValues
	public float CalculateTimeToNextSpawn(int currentScore) {
		if (currentScore > scoreToTimeBoundaries[lastCalculationIndex]
			&& lastCalculationIndex < (timeToNextSpawnValues.Length - 1)) {
			lastCalculationIndex++;
		}
		return timeToNextSpawnValues [lastCalculationIndex];
	}

	public void ResetCalculator() {
		lastCalculationIndex = 0;
	}
}
