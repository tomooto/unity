﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DCTouchManager : MonoBehaviour
{

	private Touch touch;
	private Vector2 touchPosition;

	void Update ()
	{
		if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
			touch = Input.GetTouch (0);
			touchPosition = Camera.main.ScreenToWorldPoint (new Vector2 (touch.position.x, touch.position.y));
			EventsHolder.ReportScreenTouchedEvent (touchPosition);
		}
	}
}
