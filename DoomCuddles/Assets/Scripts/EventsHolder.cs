﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventsHolder {

	public delegate void OtherFolkGotAwayAction ();
	public static event OtherFolkGotAwayAction OtherFolkGotAwayEvent;
	public delegate void ScreenTouchedAction(Vector2 touchPosition);
	public static event ScreenTouchedAction ScreenTouchedEvent;
	public delegate void OtherFolkCuddledAction (GameObject folkThatGotCuddled);
	public static event OtherFolkCuddledAction OtherFolkCuddledEvent;
	public delegate void ScoreChangedAction (int newScore);
	public static event ScoreChangedAction ScoreChangedEvent;
	public delegate void GetScoreAction (IHasCurrentScore gameObjectToReceiveScore);
	public static event GetScoreAction GetScoreEvent;

	public static void ReportOtherFolkGotAway() {
		if (OtherFolkGotAwayEvent != null) {
			OtherFolkGotAwayEvent ();
		}
	}

	public static void ReportScreenTouchedEvent(Vector2 touchPosition) {
		if (ScreenTouchedEvent != null) {
			ScreenTouchedEvent (touchPosition);
		}
	}

	public static void ReportOtherFolkCuddled(GameObject folkThatGotCuddled) {
		if (OtherFolkCuddledEvent != null) {
			OtherFolkCuddledEvent (folkThatGotCuddled);
		}
	}

	public static void ReportScoreChanged(int newScore) {
		if (ScoreChangedEvent != null) {
			ScoreChangedEvent (newScore);
		}
	}

	public static void GetScore(IHasCurrentScore gameObjectToReceiveScore) {
		if (GetScoreEvent != null) {
			GetScoreEvent (gameObjectToReceiveScore);
		}
	}

}
