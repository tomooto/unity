﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private float projectileVelocity = 20f;
	private int initialEnergy = 50;

	public GameObject projectilePrefab;
	public GameObject launcherPrefab;
	public Text itsOverText;
	public Text scoreText;
	public Text energyText;

	private FolkSpawner folkSpawner;
	private GameObject generatedProjectile;
	private GameObject launcher;
	private CoordinateGenerator coordinateGenerator;
	private Vector2 maxCoordinates;
	private Vector2 touchPosition;
	private Vector3 touchPosition3;
	private Vector3 projectileInitialPosition;
	private Touch touch;
	private Quaternion projectileDirection;
	private int energy;
	private int score;

	void Awake() {

		coordinateGenerator = transform.GetChild (1).gameObject.GetComponent<CoordinateGenerator> ();

		// Subscribe to events
		EventsHolder.ScreenTouchedEvent += FireProjectile;
		EventsHolder.OtherFolkCuddledEvent += ChangeScoreDueToCuddle;
		EventsHolder.GetScoreEvent += GiveCurrentScoreToObject;

		// Lay down hugger and display start menu
		projectileInitialPosition = coordinateGenerator.getGridCoordinates(new Vector3(0, -2));
		launcher = Instantiate (launcherPrefab, projectileInitialPosition, Quaternion.identity);
		folkSpawner = transform.GetChild (0).gameObject.GetComponent<FolkSpawner> ();
		DisplayMenu ();
	}

	void FireProjectile(Vector2 touchPosition) {
		touchPosition3.Set (touchPosition.x, touchPosition.y, 0);

		projectileDirection.SetFromToRotation (Vector3.up, touchPosition3 - projectileInitialPosition);

		generatedProjectile = Instantiate (projectilePrefab, projectileInitialPosition, projectileDirection);
		generatedProjectile.GetComponent<Rigidbody2D> ().velocity =
			generatedProjectile.transform.up * projectileVelocity;
		Destroy (generatedProjectile, 1.0f);
	}

	private IEnumerator RoundOver() {
		EventsHolder.OtherFolkCuddledEvent -= ChangeEnergyDueToCuddle;
		EventsHolder.OtherFolkGotAwayEvent -= DecrementEnergy;

		folkSpawner.StopSpawning ();
		itsOverText.text = "It's Over";

		yield return new WaitForSeconds (1.0f);
		itsOverText.text = "";
		yield return new WaitForSeconds (1.0f);

		DisplayMenu ();
	}

	void SetScore(int newScore) {
		score = newScore;
		scoreText.text = score.ToString ();
	}

	void SetEnergy(int newEnergy) {
		energy = newEnergy;
		if (energy <= 0) {
			energy = 0;
			StartCoroutine( RoundOver ());
		}
		energyText.text = energy.ToString ();
	}

	void ChangeScoreDueToCuddle(GameObject folkThatGotCuddled) {
		if (folkThatGotCuddled.tag == "BlueFolk") {
			SetScore (score + 1);
		}
	}

	void ChangeEnergyDueToCuddle(GameObject folkThatGotCuddled) {
		if (folkThatGotCuddled.tag == "BlueFolk") {
			SetEnergy (energy + 1);
		} else {
			SetEnergy (energy - 10);
		}
	}
		
	void DecrementEnergy() {
		SetEnergy (energy - 30);
	}

	private void DisplayMenu() {
		itsOverText.text = "Touch to Start";
		scoreText.text = "";
		energyText.text = "";
		EventsHolder.ScreenTouchedEvent += StartGame;
	}

	private void StartGame(Vector2 trashTouchPosition) {
		
		EventsHolder.ScreenTouchedEvent -= StartGame;
		EventsHolder.OtherFolkCuddledEvent += ChangeEnergyDueToCuddle;
		EventsHolder.OtherFolkGotAwayEvent += DecrementEnergy;

		itsOverText.text = "";
		SetScore (0);
		SetEnergy (initialEnergy);
		folkSpawner.StartSpawning (coordinateGenerator);
	}

	public void GiveCurrentScoreToObject(IHasCurrentScore gameObjectToReceiveScore) {
		gameObjectToReceiveScore.setCurrentScore (score);
	}
}
