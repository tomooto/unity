﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FolkSpawner : MonoBehaviour, IHasCurrentScore {

	// Children objects for composition
	private WaveGenerator waveGenerator;
	public LaunchForceCalculator launchForceCalculator;
	private NumberOfFolkCalculator numberOfFolkCalculator;
	private TimeToNextSpawnCalculator timeToNextSpawnCalculator;
	private RedFolkNumberCalculator redFolkNumberCalculator;

	private CoordinateGenerator coordinateGenerator;
	private Wave waveToSpawn;
	private GameObject spawnedFolk;
	private GameObject spawnedFolkPreview;
	private WavePacket wavePacket;
	private IEnumerator spawnerCoroutine;
	private int currentScore;
	private float timeToNextSpawn;

	public void StartSpawning(CoordinateGenerator inCoordinateGenerator) {

		// Initializations
		coordinateGenerator = inCoordinateGenerator;

		waveGenerator = transform.GetChild (0).gameObject.GetComponent<WaveGenerator> ();
		launchForceCalculator = transform.GetChild (1).gameObject.GetComponent<LaunchForceCalculator> ();
		launchForceCalculator.setCoordinateGenerator (coordinateGenerator);
		numberOfFolkCalculator = transform.GetChild (2).gameObject.GetComponent<NumberOfFolkCalculator> ();
		timeToNextSpawnCalculator = transform.GetChild (3).gameObject.GetComponent<TimeToNextSpawnCalculator> ();
		redFolkNumberCalculator = transform.GetChild (4).gameObject.GetComponent<RedFolkNumberCalculator> ();

		spawnerCoroutine = SpawnFolk ();
		StartCoroutine (spawnerCoroutine);
	}

	public void StopSpawning() {
		StopCoroutine (spawnerCoroutine);
		numberOfFolkCalculator.ResetCalculator ();
		timeToNextSpawnCalculator.ResetCalculator ();
		redFolkNumberCalculator.ResetCalculator ();
	}

	private IEnumerator SpawnFolk() {

		while (true) {

			EventsHolder.GetScore (this);

			timeToNextSpawn = timeToNextSpawnCalculator.CalculateTimeToNextSpawn (currentScore);

			waveToSpawn = waveGenerator.generateWave (numberOfFolkCalculator.CalculateNumberOfFolk(currentScore),
				redFolkNumberCalculator.CalculateNumberOfRedFolk(currentScore), timeToNextSpawn, 2f);

			for (int i = 0; i < waveToSpawn.wavePackets.Count; i++) {
				wavePacket = waveToSpawn.wavePackets [i];

				// Preview folk to spawn before launching
				spawnedFolkPreview = Instantiate(wavePacket.otherFolkPreview,
					coordinateGenerator.getGridCoordinates(
						new Vector2(wavePacket.coordinates.x, wavePacket.coordinates.y - 0.5f)),
					Quaternion.identity);
				spawnedFolkPreview.GetComponent<OtherFolkPreview> ().BeginCountdown (0.25f);

				yield return new WaitForSeconds (0.25f);

				// Spawn folk and cause them to launch
				spawnedFolk = Instantiate (wavePacket.otherFolkToSpawn,
					coordinateGenerator.getGridCoordinates(wavePacket.coordinates), Quaternion.identity);
				spawnedFolk.GetComponent<IOtherFolk> ().SetLaunchForceCalculator (launchForceCalculator);
				spawnedFolk.GetComponent<IOtherFolk> ().BeginCountdown (wavePacket.timeToGetAway);

				yield return new WaitForSeconds (wavePacket.timeToNextSpawn);
			}

			yield return new WaitForSeconds(timeToNextSpawn * 2);
		}
	}
		
	public void setCurrentScore(int inCurrentScore) {
		currentScore = inCurrentScore;
	}
}
