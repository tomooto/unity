﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordinateGenerator: MonoBehaviour {

	Vector2 maxCoordinates;

	void Awake() {
		maxCoordinates = Camera.main.ScreenToWorldPoint (new Vector2 (Screen.width, Screen.height));
		Debug.Log ("Max coordinates are: " + maxCoordinates.ToString ());
	}

	public Vector2 getGridCoordinates(float inX, float inY)
	{
		return new Vector2 ((float)(maxCoordinates.x / 2f * inX * 0.8), (float)(maxCoordinates.y / 3f * inY * 0.8));
	}

	public Vector3 getGridCoordinates(Vector3 inVector) {
		return new Vector3((float)(maxCoordinates.x / 2f * inVector.x * 0.8),
			(float)(maxCoordinates.y / 3f * inVector.y * 0.8), inVector.z);
	}

	public Vector2 getMaxCoordinates() {
		return maxCoordinates;
	}
}
