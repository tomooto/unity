﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave {
	
	public List<WavePacket> wavePackets;

	public Wave() {
		wavePackets = new List<WavePacket> ();
	}

}

public class WavePacket {

	public Vector3 coordinates;
	public GameObject otherFolkPreview;
	public GameObject otherFolkToSpawn;
	public float timeToNextSpawn;
	public float timeToGetAway; // Amount of time other folk will stay on screen

	public WavePacket(Vector3 inCoordinates, GameObject inPrefab, GameObject inPreview,
		float inTimeToNextSpawn, float inTimeToGetAway) {
		coordinates = inCoordinates;
		otherFolkToSpawn = inPrefab;
		otherFolkPreview = inPreview;
		timeToNextSpawn = inTimeToNextSpawn;
		timeToGetAway = inTimeToGetAway;
	}

}