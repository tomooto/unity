﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOtherFolk {

	void SetLaunchForceCalculator (LaunchForceCalculator inLaunchForceCalculator);

	void BeginCountdown (float timeToGetAway);

}
