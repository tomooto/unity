﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHasCurrentScore {
	void setCurrentScore(int currentScore);
}
