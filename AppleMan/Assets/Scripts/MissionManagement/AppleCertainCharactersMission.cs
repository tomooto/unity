﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AppleCertainCharactersMission : MonoBehaviour {

	private string certainCharacter;
	private int appledCount;
	private int countForSuccess;

	private IOtherCharacter tempOtherCharacter;

	public void setOptions(string inCertainCharacter, int inCountForSuccess) {
		certainCharacter = inCertainCharacter;
		countForSuccess = inCountForSuccess;
		appledCount = 0;
		EventHandler.CharacterAppledEvent += checkMissionObjective;
	}

	public void checkMissionObjective(GameObject characterAppled) {
		tempOtherCharacter = characterAppled.GetComponent<IOtherCharacter> ();
		if (tempOtherCharacter != null) {
			if (tempOtherCharacter.getName() == certainCharacter) {
				appledCount++;
			}
			if (appledCount >= countForSuccess) {
				Debug.Log ("Appled the character enough times!  Hooray!");
				appledCount = 0;
			}
		}
	}

	void OnDestroy() {
		EventHandler.CharacterAppledEvent -= checkMissionObjective;
	}
}
