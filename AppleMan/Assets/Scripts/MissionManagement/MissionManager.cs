﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionManager : MonoBehaviour {

	public static GameObject instance = null;

	public GameObject levelCountMissionFactoryPrefab;

	private GameObject[] activeMissions = new GameObject[3];
	private LevelCountMissionFactory levelCountMissionFactory;

	void Awake() {

		if (instance == null) {
			instance = gameObject;
		} else if (instance != gameObject) {
			Destroy (gameObject);
		}

		EventHandler.MissionCompletedEvent += handleCompletedMission;
		levelCountMissionFactory = levelCountMissionFactoryPrefab.GetComponent<LevelCountMissionFactory>();
		activeMissions [0] = levelCountMissionFactory.generateMission (0);
		activeMissions [1] = levelCountMissionFactory.generateMission (1);
		activeMissions [2] = levelCountMissionFactory.generateMission (2);

	}

	public void handleCompletedMission(int missionIndex) {
		Destroy (activeMissions [missionIndex]);
	}

	void OnDestroy() {
		EventHandler.MissionCompletedEvent -= handleCompletedMission;
	}
		
}
