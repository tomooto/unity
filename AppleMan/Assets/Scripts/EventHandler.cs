﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : MonoBehaviour {

	private List<IApplerMovementObserver> applerMovementObservers = new List<IApplerMovementObserver>();
	private List<IApplerRejectedObserver> applerRejectedObservers = new List<IApplerRejectedObserver> ();
	public delegate void ScreenTouchedAction(Vector2 touchPosition);
	public static event ScreenTouchedAction ScreenTouchedEvent;
	public delegate void CharacterAppledAction(GameObject characterThatGotAppled);
	public static event CharacterAppledAction CharacterAppledEvent;
	public delegate void LevelCompletedAction (int newLevelNum);
	public static event LevelCompletedAction LevelCompletedEvent;
	public delegate void MissionCompletedAction (int missionIndex);
	public static event MissionCompletedAction MissionCompletedEvent;

	// Methods for observers that need to know Appler's position

	public void RegisterApplerMovementObserver (IApplerMovementObserver objectToRegister) {
		applerMovementObservers.Add(objectToRegister);
	}

	public void UnregisterApplerMovementObserver(IApplerMovementObserver objectToUnregister) {
		applerMovementObservers.Remove (objectToUnregister);
	}

	public void NotifyApplerMovementObservers(Vector3 applerNewPosition) {
		for (int i = 0; i < applerMovementObservers.Count; i++) {
			applerMovementObservers[i].updateFromApplerMovement(applerNewPosition);
		}
	}

	// Method for observers that are watching for when appler is rejected

	public void RegisterApplerRejectedObserver (IApplerRejectedObserver objectToRegister) {
		applerRejectedObservers.Add (objectToRegister);
	}

	public void UnregisterApplerRejectedObserver (IApplerRejectedObserver objectToUnregister) {
		applerRejectedObservers.Remove (objectToUnregister);
	}

	public void NotifyApplerRejectedObservers() {
		for (int i = 0; i < applerRejectedObservers.Count; i++) {
			applerRejectedObservers [i].UpdateApplerWasRejected ();
		}
	}

	// Accessor methods so that other classes can trigger events

	public void ReportScreenTouched(Vector2 reportedTouchPosition) {
		if (ScreenTouchedEvent != null) {
			ScreenTouchedEvent (reportedTouchPosition);
		}
	}

	public void ReportCharacterAppled(GameObject reportedCharacter) {
		if (CharacterAppledEvent != null) {
			CharacterAppledEvent (reportedCharacter);
		}
	}

	public void ReportLevelCompleted(int reportedLevelCompleted) {
		if (LevelCompletedEvent != null) {
			LevelCompletedEvent (reportedLevelCompleted);
		}
	}

	public static void ReportMissionCompleted(int reportedMissionIndex) {
		if (MissionCompletedEvent != null) {
			MissionCompletedEvent (reportedMissionIndex);
		}
	}
		
}
