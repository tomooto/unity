﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchResponseGenerator : MonoBehaviour, IHasOptions, IScreenTouchedObserver {

	private LineRenderer lineRenderer;

	private float initialRadius = 0.3f;
	private float finalRadius = 0.6f; // Radius that response ring will go to
	private int numPositions = 10; // Number of segments used to create response circle
	private float radius;
	private Vector3 tempVector = new Vector3 (0, 0, 0);
	private float inversePositionCount;
	private float[] angleArrayX; // Array of angles to be used when drawing the circle
	private float[] angleArrayY; // Array for the y direction
	private float t; // Interpolation value for color fade
	private IEnumerator waitToResetLineRendererCoroutine = null;

	private EventHandler eventHandler;

	void Start () {
		lineRenderer = transform.GetChild (0).gameObject.GetComponent<LineRenderer> ();
		angleArrayX = new float[numPositions];
		angleArrayY = new float[numPositions];
		inversePositionCount = 1.0f / numPositions;

		for (int i = 0; i < numPositions; i++) {
			angleArrayX [i] = Mathf.Cos (360.0f * inversePositionCount * i * Mathf.Deg2Rad);
			angleArrayY [i] = Mathf.Sin (360.0f * inversePositionCount * i * Mathf.Deg2Rad);
		}
	}

	public void SetOptions(CharacterOptions characterOptions) {
		eventHandler = characterOptions.eventHandler;
		EventHandler.ScreenTouchedEvent += UpdateScreenTouched;
	}

	public void UpdateScreenTouched(Vector2 touchPosition) {
		
		if (waitToResetLineRendererCoroutine != null) {
			StopCoroutine (waitToResetLineRendererCoroutine);
		}
		
		lineRenderer.positionCount = numPositions;
		radius = initialRadius;

		waitToResetLineRendererCoroutine = WaitToResetLineRenderer (touchPosition);
		StartCoroutine (waitToResetLineRendererCoroutine);
	}

	IEnumerator WaitToResetLineRenderer (Vector2 touchPosition) {
	
		Color tempColor = Color.gray;
		tempColor.a = 0.4f;
		t = 0;

		while (t < 1.0) {

			// Expand ring

			radius = Mathf.Lerp (radius, finalRadius, t);

			for (int i = 0; i < numPositions; i++) {
				tempVector.x = radius * angleArrayX[i] + touchPosition.x;
				tempVector.y = radius * angleArrayY[i] + touchPosition.y;
				lineRenderer.SetPosition (i, tempVector);
			}

			// Fade out line

			tempColor.a = Mathf.Lerp (tempColor.a, 0, t);

			lineRenderer.startColor = tempColor;
			lineRenderer.endColor = tempColor;

			t += Time.deltaTime;

			yield return null;
		}

		lineRenderer.positionCount = 0;
	}

	void OnDestroy() {
		EventHandler.ScreenTouchedEvent -= UpdateScreenTouched;
	}

}
