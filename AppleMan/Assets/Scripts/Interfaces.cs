﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHasOptions
{
	void SetOptions (CharacterOptions characterOptions);
}

public interface IOtherCharacter // Used to verify specific characters during events
{
	string getName();
}
	
public interface IApplerMovementObserver // Observer that needs to know Appler's latest position
{
	void updateFromApplerMovement (Vector3 applerNewPosition);
}

public interface IEnemyAppledObserver { // Observer that gets notified when an enemy is hit
	void UpdateEnemyGotAppled (GameObject enemyThatGotAppled);
}

public interface IApplerRejectedObserver { // Gets notified when appler is rejected
	void UpdateApplerWasRejected ();
}

public interface IScreenTouchedObserver { // Gets notified when screen is touched
	void UpdateScreenTouched(Vector2 touchPosition);
}
	
public interface IMissionFactory // Generates and returns missions who know which index in the mission array they are
{
	GameObject generateMission (int missionIndex);
}