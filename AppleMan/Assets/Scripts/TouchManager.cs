﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour, IHasOptions {

	public bool isGamePaused = false;

	private Touch touch;
	private Vector2 touchPosition;

	private EventHandler eventHandler;

	public void SetOptions(CharacterOptions characterOptions) {
		eventHandler = characterOptions.eventHandler;
	}

	void Update () {
		if (!isGamePaused) {
			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
				touch = Input.GetTouch (0);
				touchPosition = Camera.main.ScreenToWorldPoint (new Vector2 (touch.position.x, touch.position.y));
				eventHandler.ReportScreenTouched (touchPosition);
			}
		}
	}
}
