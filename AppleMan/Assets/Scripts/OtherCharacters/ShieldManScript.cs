﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManScript : MonoBehaviour, IHasOptions, IOtherCharacter {

	private float moveTime = 0.1f;           //Time it will take object to move, in seconds.
	private float rotateScaleFactor = 500f; 	//The larger this is, the faster SM rotates

	private Rigidbody2D rb2D;               //The Rigidbody2D component attached to this object.
	private float inverseMoveTime;          //Used to make movement more efficient.
	private bool isMoving = false;
	private Vector2 pointA;
	private Vector2 pointB = Vector2.zero;
	private Quaternion rotateToHere;
	private Vector3 endPoint; // Point that will be traveled to

	private ParticleSystem dustClouds;

	//Protected, virtual functions can be overridden by inheriting classes.
	void Start ()
	{

		//Get a component reference to this object's Rigidbody2D
		rb2D = GetComponent <Rigidbody2D> ();

		//By storing the reciprocal of the move time we can use it by multiplying instead of dividing, this is more efficient.
		inverseMoveTime = 1f / moveTime;

		pointA = transform.position;

		dustClouds = transform.GetChild (0).gameObject.GetComponent<ParticleSystem> ();

		StartCoroutine (SmoothMovement ());

	}

	public void SetOptions (CharacterOptions characterOptions) {
		pointB = characterOptions.enemyInfo.pointB;

		// Set the rotation to point towards the target point
		transform.up = new Vector3(pointB.x, pointB.y, 0) - transform.position;
	}

	public string getName() {
		return "ShieldMelon";
	}

	protected IEnumerator SmoothMovement ()
	{
		while (true) {

			if (((Vector2)transform.position - pointA).sqrMagnitude < float.Epsilon) {
				endPoint = new Vector3 (pointB.x, pointB.y, 0);
			} else {
				endPoint = new Vector3 (pointA.x, pointA.y, 0);
			}
			
			float sqrRemainingDistance = (transform.position - endPoint).sqrMagnitude;

			dustClouds.Emit (5);

			//While that distance is greater than a very small amount (Epsilon, almost zero):
			while (sqrRemainingDistance > 0.1f) {

				//Find a new position proportionally closer to the end, based on the moveTime
				Vector3 newPostion = Vector3.MoveTowards (rb2D.position, endPoint, inverseMoveTime * Time.deltaTime);

				//Call MovePosition on attached Rigidbody2D and move it to the calculated position.
				rb2D.MovePosition (newPostion);

				//Recalculate the remaining distance after moving.
				sqrRemainingDistance = (transform.position - endPoint).sqrMagnitude;

				//Return and loop until sqrRemainingDistance is close enough to zero to end the function
				yield return null;
			}

			// Set the position to the endpoint after the smooth movement just for safety
			transform.position = endPoint;

			rotateToHere = transform.rotation * Quaternion.Euler (0, 0, 180f);

			yield return new WaitForSeconds (0.5f);

			// Rotate 180 over time
			while (Quaternion.Angle (transform.rotation, rotateToHere) > 0.5f) {

				transform.rotation = Quaternion.RotateTowards (transform.rotation, rotateToHere,
					rotateScaleFactor * Time.deltaTime);

				yield return null;
			}

			// Hard set the rotation after smooth movement just for safety
			transform.rotation = rotateToHere;

			yield return new WaitForSeconds (0.5f);
		}

	}
}
