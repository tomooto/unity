﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoutMan : MonoBehaviour, IApplerMovementObserver, IHasOptions, IOtherCharacter {

	public GameObject shoutSoundWavePrefab;

	private float shoutVelocity = 6f;
	private GameObject generatedShout;
	private EventHandler eventHandler;
	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		StartCoroutine (Shout());
	}

	public void SetOptions(CharacterOptions characterOptions) {
		transform.up = characterOptions.applerRef.transform.position - transform.position;
		eventHandler = characterOptions.eventHandler;
		eventHandler.RegisterApplerMovementObserver (this);
	}

	public string getName() {
		return "ShoutBerry";
	}

	public void updateFromApplerMovement(Vector3 applerNewPosition) {
		transform.up = applerNewPosition - transform.position;
	}

	// Routine that handles shouts, including wait time
	protected IEnumerator Shout () {

		while (true) {
			
			yield return new WaitForSeconds (2f);

			animator.SetTrigger ("ShoutTrigger");

			// Wait for 80% of the animation to finish before emitting shout
			yield return new WaitForSeconds (animator.GetCurrentAnimatorStateInfo(0).length * 0.8f);

			generatedShout = Instantiate (shoutSoundWavePrefab, transform.position, transform.rotation, transform.parent);

			generatedShout.GetComponent<Rigidbody2D> ().velocity = generatedShout.transform.up * shoutVelocity;

			Destroy (generatedShout, 3f);

		}
	}

	// kill shout when man is killed
	void OnDestroy() {
		eventHandler.UnregisterApplerMovementObserver (this);
		Destroy (generatedShout);
	}
}
