﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo {

	public Vector3 location;
	public float angle;
	public GameObject prefab;
	public Vector2 pointB; // Vector used when a character moves from one point to another (aka ShieldMelon)

	public EnemyInfo (Vector2 inLoc, float inAngle, GameObject inPrefab, Vector2 inPointB = default(Vector2)){
		location = new Vector3(inLoc.x, inLoc.y, 0);
		angle = inAngle;
		prefab = inPrefab;
		pointB = inPointB;
	}
}

public class Level
{
	public List<EnemyInfo> enemies;
	public int numEnemies;

	public Level(List<EnemyInfo> inEnemies){
		enemies = inEnemies;
		numEnemies = inEnemies.Count;
	}
}

public class CharacterOptions {

	public EnemyInfo enemyInfo;
	public GameObject applerRef;
	public EventHandler eventHandler;

	public CharacterOptions() {
		enemyInfo = null;
		applerRef = null;
		eventHandler = null;
	}
}
