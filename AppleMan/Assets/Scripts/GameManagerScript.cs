﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManagerScript : MonoBehaviour, IApplerRejectedObserver {

	public Vector2 maxCoordinates;

	public GameObject appleManPrefab;
	public GameObject shieldManPrefab;
	public GameObject circleSwordManPrefab;
	public GameObject shoutManPrefab;
	public GameObject shoutSoundWavePrefab;
	public GameObject auditorPrefab;
	public GameObject seekerMelonPrefab;
	public GameObject peaLongPrefab;
	public GameObject peaMediumPrefab;
	public GameObject peaShortPrefab;

	public Text levelText;
	public Text countdownText;
	public Text rejectionText;
	public Text rejectionDescriptionText;

	public EventHandler eventHandler;
	public MissionManager missionManager;
	public TouchManager touchManager;
	private TouchResponseGenerator touchResponseGenerator;
	public static GameObject instance = null;

	public bool appleManIsDead = false;
	public bool enemyIsDead = false;
	private float timeToNextScene = 1f;
	private GameObject appleMan = null;
	private bool isLoadingNewScene = false;

	private GameObject[] enemies = new GameObject[20];
	private int levelNum = 0;
	private int currNumEnemies = 0;
	public List<Level> beginningLevels = new List<Level>();
	public List<Level> mediumLevels = new List<Level>();
	public List<Level> lateLevels = new List<Level>();
	public List<Level> bossLevels = new List<Level>();
	private int levelIndex = 0; // Stores random number to compare and then instantiate next level
	private int lastLevelIndex = 20; // Index of last level instantiated (prevents repeat levels)
	private float[] levelSelectIntervals; // Intervals used to select levelIndex
	private float[] levelSelectWeights; // Weights to determine levelIndex
	private Level levelToInstantiate;
	private CharacterOptions characterOptions = new CharacterOptions();


	// Use this for initialization
	void Awake () {

		if (instance == null) {
			instance = gameObject;
		} else if (instance != gameObject) {
			Destroy (gameObject);
		}

		// Set up event handler
		eventHandler = transform.GetChild (0).gameObject.GetComponent<EventHandler> ();
		EventHandler.CharacterAppledEvent += UpdateEnemyGotAppled;
		eventHandler.RegisterApplerRejectedObserver (this);
		characterOptions.eventHandler = eventHandler;

		// Set up Touch Manager
		touchManager = transform.GetChild (1).gameObject.GetComponent<TouchManager> ();
		touchManager.SetOptions (characterOptions);

		// Instantiate Mission Manager
		missionManager = transform.GetChild (2).gameObject.GetComponent<MissionManager> ();

		// Set up Touch response generator
		touchResponseGenerator = transform.GetChild(3).gameObject.GetComponent<TouchResponseGenerator>();
		touchResponseGenerator.SetOptions (characterOptions);

		// Miscellaneous initializations
		DontDestroyOnLoad (gameObject);
		levelText.text = "";
		countdownText.text = "";
		rejectionText.text = "";
		rejectionDescriptionText.text = "";
		maxCoordinates = Camera.main.ScreenToWorldPoint (new Vector2 (Screen.width, Screen.height));

		// Set up levels and levelSelect weights
		LevelInstantiator.InstantiateLevelArrays (this);
		levelSelectWeights = new float[lateLevels.Count];
		levelSelectIntervals = new float[lateLevels.Count];
		for (int i = 0; i < levelSelectWeights.Length; i++) {
			levelSelectWeights [i] = 100f;
		}

		StartCoroutine(LoadNextLevel(true));
		
	}

	public void UpdateApplerWasRejected() {
		
		touchManager.isGamePaused = true;
		Time.timeScale = 0;

		StartCoroutine( GameOver ());

	}

	IEnumerator GameOver() {

		// Wait a moment for touches count to clear
		float nextEndTime = Time.realtimeSinceStartup + 0.4f;
		while (Time.realtimeSinceStartup < nextEndTime)
		{
			yield return null;
		}

		// Display Message (after the breather)
		rejectionText.text = "Rejected";
		rejectionDescriptionText.text = "Touch anywhere\nto try again";

		// Wait until the player touches the screen
		while (Input.touchCount == 0) {
			yield return null;
		}

		// Unpause and start a new level
		rejectionText.text = "";
		rejectionDescriptionText.text = "";
		touchManager.isGamePaused = false;
		Time.timeScale = 1;
		Destroy (appleMan);
		StartCoroutine (LoadNextLevel (true));
	}

	public void UpdateEnemyGotAppled (GameObject enemyThatGotAppled) {
		if (enemyThatGotAppled.tag == "OtherCharacter" || enemyThatGotAppled.tag == "CharacterLeader") {
			Destroy (enemyThatGotAppled);
			currNumEnemies--;
			if (currNumEnemies <= 0) {
				// Level is won.  Notify event listeners and load next level
				eventHandler.ReportLevelCompleted(levelNum - 1);
				StartCoroutine (LoadNextLevel (false));
			}
		}
	}

	IEnumerator LoadNextLevel (bool isStartingOver) {

		yield return new WaitForSeconds (1f);

		ReinstantiateMap (isStartingOver);

		// Pause the game until countdown has finished

		levelText.text = "Level\n" + levelNum;
		countdownText.text = "Ready";

		touchManager.isGamePaused = true;
		Time.timeScale = 0;

		float nextEndTime = Time.realtimeSinceStartup + 0.4f;
		while (Time.realtimeSinceStartup < nextEndTime)
		{
			yield return null;
		}
			
		countdownText.text = "Apple";
		nextEndTime = Time.realtimeSinceStartup + 0.4f;
		while (Time.realtimeSinceStartup < nextEndTime)
		{
			yield return null;
		}

		// Increment counter for next round
		levelNum++;

		// Start the round
		levelText.text = "";
		countdownText.text = "";
		touchManager.isGamePaused = false;
		Time.timeScale = 1;
	}

	void ReinstantiateMap (bool isStartingOver) {

		appleManIsDead = false;
		enemyIsDead = false;

		if (isStartingOver) {
			levelNum = 1;
		}

		Destroy (appleMan);
		for (int i = 0; i < enemies.Length; i++) {
			Destroy (enemies [i]);	
		}

		timeToNextScene = 1f;

		// Spawn AppleMan
		appleMan = Instantiate (appleManPrefab, new Vector3 (0, -2, 0), Quaternion.identity, transform);
		appleMan.GetComponent<Movement> ().SetOptions (eventHandler);
		characterOptions.applerRef = appleMan;

		// Select level to instantiate
		if (levelNum > 0 && levelNum <= 4) {
			ChooseLevelIndex (beginningLevels.Count, false);
			levelToInstantiate = beginningLevels [levelIndex];
		} else if (levelNum > 4 && levelNum <= 9) {
			ChooseLevelIndex (mediumLevels.Count, false);
			levelToInstantiate = mediumLevels [levelIndex];
		} else if (levelNum % 10 == 0) {
			levelToInstantiate = bossLevels [Random.Range (0, bossLevels.Count)];
		} else {
			ChooseLevelIndex (lateLevels.Count, true);
			levelToInstantiate = lateLevels [levelIndex];
		}

		// Spawn all enemies and set their properties
		currNumEnemies = levelToInstantiate.numEnemies;
		for (int i = 0; i < currNumEnemies; i++) {
			EnemyInfo tempEnemyInfo = levelToInstantiate.enemies [i];
			characterOptions.enemyInfo = tempEnemyInfo;
			enemies [i] = Instantiate (tempEnemyInfo.prefab, tempEnemyInfo.location,
				Quaternion.AngleAxis(tempEnemyInfo.angle, Vector3.forward), transform);
			IHasOptions tempScriptRef = enemies [i].GetComponent<IHasOptions> ();
			if (tempScriptRef != null) {
				tempScriptRef.SetOptions (characterOptions);
			}
		}

	}

	void ChooseLevelIndex(int numLevels, bool isLateLevel) {
		// Use weighted interval algorithm for late levels
		if (isLateLevel) {
			levelSelectIntervals [0] = levelSelectWeights [0];
			for (int i = 1; i < numLevels; i++) {
				levelSelectIntervals[i] = levelSelectIntervals[i-1] + levelSelectWeights[i];
			}
			float tempSelector = Random.Range (0, levelSelectIntervals [levelSelectIntervals.Length - 1]);
			bool foundLevelIndex = false;
			for (int i = 0; i < numLevels - 1; i++) {
				if (tempSelector < levelSelectIntervals [i]) {
					levelIndex = i;
					foundLevelIndex = true;
					break;
				}
			}
			if (!foundLevelIndex) {
				levelIndex = numLevels - 1;
			}
			levelSelectWeights [levelIndex] = levelSelectWeights [levelIndex] * 0.5f;
		// Just don't repeat level for everything else
		} else {
			levelIndex = Random.Range (0, numLevels);
			if (numLevels > 1) {
				while (levelIndex == lastLevelIndex) {
					levelIndex = Random.Range (0, numLevels);
				}
			}
			lastLevelIndex = levelIndex;
		}
	}

	//
	// Put this in its own function library
	//
	public Vector2 getGridCoordinates(float inX, float inY)
	{
		return new Vector2 ((float)(maxCoordinates.x / 2f * inX * 0.8), (float)(maxCoordinates.y / 3f * inY * 0.8));
	}
		
}
